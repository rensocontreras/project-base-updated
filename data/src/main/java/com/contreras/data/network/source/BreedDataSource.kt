package com.contreras.data.network.source

import com.contreras.data.network.base.BaseResponse
import com.contreras.data.network.remote.api.BreedApi
import com.contreras.data.network.remote.retrofit.NetworkHandler
import com.contreras.data.network.response.BreedResponse
import com.contreras.domain.extra.Result
import retrofit2.Retrofit

/** Created by Renso Contreras on 23/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

interface BreedDataSource {

    suspend fun getListBreeds(): Result<BaseResponse<List<BreedResponse>>>

    class BreedDataSourceImpl(retrofit: Retrofit, private val networkHandler: NetworkHandler): BreedDataSource {
        private val api by lazy { retrofit.create(BreedApi::class.java) }

        override suspend fun getListBreeds(): Result<BaseResponse<List<BreedResponse>>> {
            return networkHandler.handleServerResponse { api.getListBreeds() }
        }
    }
}