package com.contreras.baseproject.mapper

import com.contreras.baseproject.model.Breed
import com.contreras.domain.model.BreedEntity

/** Created by Renso Contreras on 23/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

class BreedMapperImpl: BreedMapper {
    override suspend fun breedDomainToApp(breedEntity: BreedEntity?): Breed? {
        return breedEntity?.let{
            Breed(
                id = breedEntity.id,
                name = breedEntity.name
            )
        }
    }

    override suspend fun listBreedDomainToApp(listBreedEntity: List<BreedEntity>?): List<Breed>? {
        return listBreedEntity?.mapNotNull { breedDomainToApp(it) }
    }
}