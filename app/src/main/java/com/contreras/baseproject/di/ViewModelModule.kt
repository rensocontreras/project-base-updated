package com.contreras.baseproject.di

import com.contreras.baseproject.ui.breed.BreedViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/** Created by Renso Contreras on 23/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

val viewModelModule = module {
    viewModel { BreedViewModel(get(), get()) }
}