package com.contreras.domain.usecase

import com.contreras.domain.extra.Result
import com.contreras.domain.model.BreedEntity
import com.contreras.domain.repository.BreedRepository

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

class GetListBreedUseCase(private val repository: BreedRepository){
    suspend fun getListBreeds(): Result<List<BreedEntity>>{
        return repository.getListBreeds()
    }
}