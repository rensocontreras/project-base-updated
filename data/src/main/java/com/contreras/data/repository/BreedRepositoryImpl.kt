package com.contreras.data.repository

import com.contreras.data.network.mapper.BreedMapper
import com.contreras.data.network.remote.retrofit.NetworkHandler.Companion.FAILURE_DATA_NULL_ERROR
import com.contreras.data.network.source.BreedDataSource
import com.contreras.domain.extra.FailureDataNullError
import com.contreras.domain.extra.Result
import com.contreras.domain.model.BreedEntity
import com.contreras.domain.repository.BreedRepository

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

class BreedRepositoryImpl(private val source: BreedDataSource, private val mapper: BreedMapper) : BreedRepository {
    override suspend fun getListBreeds(): Result<List<BreedEntity>> {
        return when(val result = source.getListBreeds()){
            is Result.Success ->{
                result.response?.data?.let{
                    Result.Success(mapper.breedListToDataToDomain(it))
                }?: run {
                    Result.Failure(FailureDataNullError(FAILURE_DATA_NULL_ERROR))
                }
            }
            is Result.Failure -> result
        }
    }

}