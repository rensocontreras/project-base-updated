package com.contreras.data.network.response

import com.contreras.domain.model.BreedEntity
import com.google.gson.annotations.SerializedName

/** Created by Renso Contreras on 23/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

//typealias GetListBreedsServerResponse = BaseResponse<List<UnitDataResponse>>

data class BreedResponse(
    @SerializedName("id") val id: String?,
    @SerializedName("name") val name: String?){

    companion object {
        fun toBreedEntity(response: BreedResponse): BreedEntity {
            return BreedEntity(
                id = response.id,
                name = response.name?:"")
        }

        fun toListBreedEntity(listReponse: List<BreedResponse>): List<BreedEntity>{
            val mList = mutableListOf<BreedEntity>()
            for(i in listReponse){
                mList.add(BreedEntity(
                    id = i.id,
                    name = i.name?:""))
            }
            return mList.toList()
        }
    }

}