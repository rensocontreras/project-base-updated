package com.contreras.data.network.base

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

class ErrorResponse(status: Int, message: String) : BaseResponse<Nothing>(status, message,null)