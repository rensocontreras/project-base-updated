package com.contreras.baseproject.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.reflect.KClass

/** Created by Renso Contreras on 29/11/2020.
 * rensocontreras91@peruapps.com.pe
 * Lima, Peru.
 **/

abstract class  BaseFragment<T: ViewDataBinding, out ViewModelType : BaseViewModel<*>>(clazz: KClass<ViewModelType>) : Fragment() {

    protected lateinit var viewDataBinding: T
    private lateinit var rootView: View
    val myViewModel : ViewModelType by viewModel(clazz)

    abstract fun getLayoutId(): Int
    abstract fun getBindingVariable() : Int
    abstract fun initView(bundle: Bundle?)
    abstract fun setupView(bundle: Bundle?)
    abstract fun onRestoreView(savedInstanceState: Bundle)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        rootView = viewDataBinding.root
        viewDataBinding.setVariable(getBindingVariable(), myViewModel)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner
        viewDataBinding.executePendingBindings()
        initView(arguments)
        savedInstanceState?.let { onRestoreView(it) } ?: setupView(arguments)
    }

    fun showButtonNavigator() {
        val activity = requireActivity()
        if (isViewExists() && (activity is BaseActivity<*,*>)) {
            activity.showButtonNavigator()
        }
    }

    fun hideButtonNavigator() {
        val activity = requireActivity()
        if (isViewExists() && (activity is BaseActivity<*,*>)) {
            activity.hideButtonNavigator()
        }
    }

    fun isViewExists(): Boolean = (isAdded && activity != null)
}