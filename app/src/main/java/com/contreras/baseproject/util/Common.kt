package com.contreras.baseproject.util

import android.content.Context
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.contreras.baseproject.base.BaseActivity
import com.contreras.baseproject.base.BaseFragment
import com.contreras.baseproject.base.BaseViewModel

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/


fun String.isValidEmail():Boolean= Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun View.gone() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun Context.toast(message: String, duration: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this, message, duration).show()
}

fun BaseActivity<*,*>.registerErrorObserver(viewModel: BaseViewModel<*>) {
    viewModel.onErrorMessage.observe(this, {
        toast(it)
    })
}

fun BaseActivity<*, *>.seeButtonNavigatorObserver(viewModel: BaseViewModel<*>) {
    viewModel.seeButtonNavigator.observe(this, { isVisible ->
        if (isVisible) showButtonNavigator() else hideButtonNavigator()
    })
}

fun BaseFragment<*, *>.seeButtonNavigatorObserver(viewModel: BaseViewModel<*>) {
    viewModel.seeButtonNavigator.observe(viewLifecycleOwner, { isVisible ->
        if (isVisible) showButtonNavigator() else hideButtonNavigator()
    })
}


