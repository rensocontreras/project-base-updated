package com.contreras.baseproject.mapper

import com.contreras.baseproject.model.Breed
import com.contreras.domain.model.BreedEntity

/** Created by Renso Contreras on 23/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

interface BreedMapper {
    suspend fun breedDomainToApp(breedEntity: BreedEntity?) : Breed?
    suspend fun listBreedDomainToApp(listBreedEntity: List<BreedEntity>?): List<Breed>?
}