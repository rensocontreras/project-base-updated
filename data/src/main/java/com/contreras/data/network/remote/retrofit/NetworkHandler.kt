package com.contreras.data.network.remote.retrofit

import com.contreras.data.BuildConfig
import com.contreras.data.network.remote.utils.ConnectionUtils
import com.contreras.data.network.base.BaseResponse
import com.contreras.data.network.base.ErrorResponse
import com.google.gson.Gson
import retrofit2.Response
import java.lang.Exception
import com.contreras.domain.extra.Result
import com.contreras.domain.extra.FailureNetwork
import com.contreras.domain.extra.FailureUnauthorized
import com.contreras.domain.extra.FailureServer
import com.contreras.domain.extra.FailureTimeout
import com.contreras.domain.extra.FailureNetworkConnectionLostSuddenly
import com.contreras.domain.extra.FailureNone
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

class NetworkHandler(val networkUtils: ConnectionUtils) {

    companion object {
        const val FAILURE_NETWORK_CONNECTION_ERROR         = "Por favor verifique su conexión a internet e inténtelo nuevamente."
        const val FAILURE_DEFAULT_ERROR                    = "Ha ocurrido un problema, vuelva a intentarlo!"
        const val FAILURE_UNAUTHORIZED_ERROR               = "Su sesión ha expirado. Por favor vuelva a iniciar sesión."
        const val FAILURE_TIMEOUT                          = "No se pudo comunicar con el sistema, vuelve a intentarlo."
        const val FAILURE_NETWORK_CONNECTION_LOST_SUDDENLY = "La conexión se perdió de repente. Comprueba el wifi o los datos móviles."
        const val FAILURE_DATA_NULL_ERROR                  = "¡Ocurrió un error!"
        const val FAILURE_APPLICATION_ERROR                = "Error de la aplicación"
        const val FAILURE_NONE                             = "Ha ocurrido un error!"
    }

    val defaultErrorResult: Result.Failure
        get() = Result.Failure(FailureServer(FAILURE_DEFAULT_ERROR))

    inline fun <T> handleServerResponse(task: () -> Response<BaseResponse<T>>): Result<BaseResponse<T>> {
        when(networkUtils.isNetworkAvailable()){
            true -> try {
                val response = task()
                if (response.isSuccessful) {
                    response.body()?.let{
                        if (it.isProcessedSuccessfully)
                            return Result.Success(it)

                        if (it.status == 401)
                            return Result.Failure(FailureUnauthorized(if (it.message.isNotBlank()) it.message else FAILURE_UNAUTHORIZED_ERROR))
                    }
                } else {
                    val errorResponseAsString= response.errorBody()?.string()
                    val errorResponse = Gson().fromJson(errorResponseAsString, ErrorResponse::class.java)

                    errorResponse?.let{
                        if (it.status == 401)
                            return Result.Failure(FailureUnauthorized(if (it.message.isNotBlank()) it.message else FAILURE_UNAUTHORIZED_ERROR))

                        if (it.message.isNotBlank())
                            return Result.Failure(FailureServer(it.message))

                    }?: run{
                        if (response.code() == 401)
                            return Result.Failure(FailureUnauthorized(FAILURE_UNAUTHORIZED_ERROR))
                    }
                }
            } catch (e: Exception) {
                return parseException(e)
            }

            else -> return Result.Failure(FailureNetwork(FAILURE_NETWORK_CONNECTION_ERROR))
        }

        return defaultErrorResult
    }

    fun parseException(throwable: Throwable) : Result.Failure {
        if(BuildConfig.DEBUG) throwable.printStackTrace()
        return when(throwable) {
            is SocketTimeoutException -> Result.Failure(FailureTimeout(FAILURE_TIMEOUT))
            is SSLException           -> Result.Failure(FailureNetworkConnectionLostSuddenly(FAILURE_NETWORK_CONNECTION_LOST_SUDDENLY))
            else                      -> Result.Failure(FailureNone(FAILURE_NONE))
        }
    }
}
