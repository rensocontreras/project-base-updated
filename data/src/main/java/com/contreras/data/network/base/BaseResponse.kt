package com.contreras.data.network.base

import com.google.gson.annotations.SerializedName

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

open class BaseResponse<T>(@SerializedName("status") val status: Int,
                           @SerializedName("message") val message: String,
                           @SerializedName("data") val data: T?) {
    val isProcessedSuccessfully
        get() = (status in 200..299)
}
