package com.contreras.baseproject.ui.breed

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.contreras.baseproject.base.BaseViewModel
import com.contreras.baseproject.mapper.BreedMapper
import com.contreras.baseproject.model.Breed
import com.contreras.baseproject.util.SingleLiveEvent
import com.contreras.domain.usecase.GetListBreedUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import com.contreras.domain.extra.*

/** Created by Renso Contreras on 23/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

class BreedViewModel(private val getListBreedUseCase: GetListBreedUseCase,
                     private val mapper: BreedMapper
): BaseViewModel<Any>() {

    private val _listBreeds= SingleLiveEvent<List<Breed>>()
    val listBreeds: LiveData<List<Breed>> = _listBreeds

    fun getListBreed(){
        startLoading()
        viewModelScope.launch {
            val listBreedsResult = withContext(Dispatchers.IO){
                getListBreedUseCase.getListBreeds()
            }
            when (listBreedsResult) {
                is Result.Success -> {
                    val listBreeds = withContext(Dispatchers.IO){
                        mapper.listBreedDomainToApp(listBreedsResult.response)
                    }
                    _listBreeds.value = listBreeds
                }
                is Result.Failure -> handleFailureResult(listBreedsResult) //showErrorMessage(listBreedsResult.exception)
            }
            stopLoading()
        }
    }
}