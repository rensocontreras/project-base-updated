package com.contreras.data.di

import com.contreras.data.network.mapper.BreedMapper
import org.koin.dsl.module

/** Created by Renso Contreras on 23/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

val mapperDataModule = module {
    single<BreedMapper> { BreedMapper.BreedMapperImpl() }
}