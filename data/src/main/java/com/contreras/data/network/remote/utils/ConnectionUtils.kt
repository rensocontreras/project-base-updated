package com.contreras.data.network.remote.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

interface ConnectionUtils {
    fun isNetworkAvailable() : Boolean

    @Suppress("DEPRECATION")
    class ConnectionUtilsImpl(private val applicationContext: Context) : ConnectionUtils {
        override fun isNetworkAvailable(): Boolean {
            try {
                val connectivityManager =
                    applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    val nw = connectivityManager.activeNetwork ?: return false
                    val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
                    return when {
                        actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                        actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                        actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                        else -> false
                    }
                } else {
                    val nwInfo = connectivityManager.activeNetworkInfo ?: return false
                    return nwInfo.isConnected
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("NetworkUtils", "Exception happened: ${e.message}")
                return false
            }
        }
    }
}
