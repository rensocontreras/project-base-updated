package com.contreras.baseproject.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

@Parcelize
data class Breed (
    var id: String? = null,
    var name: String? = null): Parcelable