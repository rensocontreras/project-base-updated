package com.contreras.data.di

import com.contreras.data.repository.BreedRepositoryImpl
import com.contreras.domain.repository.BreedRepository
import org.koin.dsl.module

/** Created by Renso Contreras on 23/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

val repositoryModule = module {
    single<BreedRepository> { BreedRepositoryImpl(get(), get()) }
}
