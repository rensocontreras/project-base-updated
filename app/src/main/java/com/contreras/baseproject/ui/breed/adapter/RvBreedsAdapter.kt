package com.contreras.baseproject.ui.breed.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import androidx.databinding.library.baseAdapters.BR
import com.contreras.baseproject.model.Breed

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

class RvBreedsAdapter(private val idItemLayout:Int,
                      private var listBreeds:List<Breed>,
                      private val itemCallback:(breed: Breed)->Unit?)
    : RecyclerView.Adapter<RvBreedsAdapter.BreedViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BreedViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, idItemLayout, parent, false)
        return BreedViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BreedViewHolder, position: Int) {
        holder.bind(listBreeds[position])
        /*
        holder.dataBinding.root.setOnClickListener {
            itemCallback(listBreeds[position])
        }
         */
    }

    override fun getItemCount(): Int {
        return listBreeds.size
    }

    fun update(data:List<Breed>){
        listBreeds= data
        notifyDataSetChanged()
    }

    fun selectItem(breed: Breed){
        itemCallback(breed)
    }

    inner class BreedViewHolder(val dataBinding: ViewDataBinding): RecyclerView.ViewHolder(dataBinding.root){
        fun bind(breed:Breed){
            dataBinding.setVariable(BR.breed, breed)
            dataBinding.executePendingBindings()
            dataBinding.root.setOnClickListener {
                selectItem(breed)
            }
        }
    }
}