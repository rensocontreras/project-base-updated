package com.contreras.baseproject.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.contreras.baseproject.util.SingleLiveEvent
import com.contreras.domain.extra.*

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

@Suppress("PropertyName", "MemberVisibilityCanBePrivate")
abstract class BaseViewModel<T> : ViewModel() {

    private val _isLoading= SingleLiveEvent<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _seeButtonNavigator = MutableLiveData(true)
    val seeButtonNavigator: LiveData<Boolean> = _seeButtonNavigator

    private val _onSuccessMessage= SingleLiveEvent<String>()
    val onSuccessMessage: LiveData<String> = _onSuccessMessage

    private val _onErrorMessage= SingleLiveEvent<String>()
    val onErrorMessage: LiveData<String> = _onErrorMessage

    protected val _unauthorizedException = SingleLiveEvent<String>()
    val unauthorizedException: LiveData<String>
        get() = _unauthorizedException

    fun handleFailureResult(failure: Result.Failure) {
        val message   = failure.exception.message
        when (val exception = failure.exception) {
            is FailureUnauthorized                  -> _unauthorizedException.value = message
            is FailureNetwork                       -> _onErrorMessage.value        = message
            is FailureServer                        -> _onErrorMessage.value        = message
            is FailureTimeout                       -> _onErrorMessage.value        = message
            is FailureDataNullError                 -> _onErrorMessage.value        = message
            is FailureDataToDomainMapper            -> _onErrorMessage.value        = message
            is FailureNetworkConnectionLostSuddenly -> _onErrorMessage.value        = message
            is FailureNone                          -> _onErrorMessage.value        = message
            else                                    ->  _onErrorMessage.value       = exception.message
        }
    }

    fun startLoading(){
        _isLoading.postValue(true)
    }

    fun stopLoading(){
        _isLoading.postValue(false)
    }

    fun showSuccessMessage(message: String?){
        _onSuccessMessage.postValue(message)
    }

    fun showErrorMessage(exception: Exception?){
        _onErrorMessage.postValue(exception?.message)
    }
}
