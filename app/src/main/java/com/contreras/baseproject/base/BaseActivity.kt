package com.contreras.baseproject.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.gu.toolargetool.TooLargeTool

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

abstract class BaseActivity<T: ViewDataBinding, out V : BaseViewModel<*>> : AppCompatActivity(){

    protected lateinit var viewDataBinding: T
    private var _viewModel: V? = null
    abstract fun getViewModel() : V

    abstract fun getIdLayout(): Int
    abstract fun getBindingVariable() : Int
    abstract fun initView(bundle: Bundle?)
    abstract fun setupView(bundle: Bundle?)
    abstract fun onRestoreView(savedInstanceState: Bundle)
    abstract fun setBottomNavigation(isShowed: Boolean)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView(this, getIdLayout())
        _viewModel = if (_viewModel == null) getViewModel() else _viewModel
        viewDataBinding.setVariable(getBindingVariable(), _viewModel)
        viewDataBinding.lifecycleOwner = this
        viewDataBinding.executePendingBindings()
        initView(intent.extras)
        savedInstanceState?.let { onRestoreView(it) } ?: setupView(intent.extras)
        TooLargeTool.startLogging(application)
    }

    fun showButtonNavigator() {
        setBottomNavigation(true)
    }

    fun hideButtonNavigator() {
        setBottomNavigation(false)
    }
}