package com.contreras.domain.di

import com.contreras.domain.usecase.GetListBreedUseCase
import org.koin.dsl.module

/** Created by Renso Contreras on 23/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

val useCasesModule = module {
    factory{ GetListBreedUseCase(get()) }
}