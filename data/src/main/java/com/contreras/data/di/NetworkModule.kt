package com.contreras.data.di

import com.contreras.data.network.remote.retrofit.ApiClient
import com.contreras.data.network.remote.retrofit.NetworkHandler
import com.contreras.data.network.remote.utils.ConnectionUtils
import com.contreras.data.network.source.BreedDataSource
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

val networkModule = module {
    single<ConnectionUtils> { ConnectionUtils.ConnectionUtilsImpl(androidContext()) }
    single { ApiClient.create() }
    single { NetworkHandler(get()) }
}
