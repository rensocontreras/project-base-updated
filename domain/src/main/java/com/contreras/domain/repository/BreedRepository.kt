package com.contreras.domain.repository

import com.contreras.domain.model.BreedEntity
import com.contreras.domain.extra.Result

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

interface BreedRepository {
    suspend fun getListBreeds(): Result<List<BreedEntity>>
}