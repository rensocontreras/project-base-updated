package com.contreras.baseproject.ui.breed

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.contreras.baseproject.R
import com.contreras.baseproject.base.BaseActivity
import com.contreras.baseproject.databinding.ActivityBreedBinding
import com.contreras.baseproject.model.Breed
import com.contreras.baseproject.ui.breed.adapter.RvBreedsAdapter
import com.contreras.baseproject.util.registerErrorObserver
import kotlinx.android.synthetic.main.activity_breed.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import com.contreras.baseproject.BR
import com.contreras.baseproject.util.Constant.Breed.BUNDLE_LIST_BREED
import com.contreras.baseproject.util.seeButtonNavigatorObserver
import com.contreras.baseproject.util.toast

class BreedActivity : BaseActivity<ActivityBreedBinding, BreedViewModel>() {

    private val breedViewModel: BreedViewModel by viewModel()
    private lateinit var rvBreedsAdapter: RvBreedsAdapter
    private var listBreeds = listOf<Breed>()

    override fun getIdLayout(): Int {
        return R.layout.activity_breed
    }

    override fun getViewModel(): BreedViewModel {
        return breedViewModel
    }

    override fun getBindingVariable(): Int{
        return BR.breedViewModel
    }

    override fun initView(bundle: Bundle?) {
        ui()
        setupViewModel()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(BUNDLE_LIST_BREED, listBreeds as ArrayList<Breed>)
    }

    override fun onRestoreView(savedInstanceState: Bundle) {
        listBreeds = savedInstanceState.getParcelableArrayList(BUNDLE_LIST_BREED)?: listOf()
        rvBreedsAdapter.update(listBreeds)
    }

    private fun setupViewModel() {
        registerErrorObserver(breedViewModel)
        seeButtonNavigatorObserver(breedViewModel)
        breedViewModel.listBreeds.observe(this, readListBreedsObserver)
    }

    override fun setupView(bundle: Bundle?) {
        breedViewModel.getListBreed()
    }

    private fun ui() {
        rvBreeds.layoutManager = LinearLayoutManager(this)
        //emptyList()   ,  arrayListOf()
        rvBreedsAdapter = RvBreedsAdapter(R.layout.item_breed, emptyList()) {
            toast(it.name?:"")
        }
        rvBreeds.adapter = rvBreedsAdapter
    }

    private val readListBreedsObserver = Observer<List<Breed>?>{
        it?.let{
            listBreeds = it
            rvBreedsAdapter.update(it)
        }
    }

    override fun setBottomNavigation(isShowed: Boolean) {
    }
}