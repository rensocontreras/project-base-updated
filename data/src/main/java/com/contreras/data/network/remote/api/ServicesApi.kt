package com.contreras.data.network.remote.api

import com.contreras.data.network.base.BaseResponse
import com.contreras.data.network.response.BreedResponse
import retrofit2.Response
import retrofit2.http.GET

/** Created by Renso Contreras on 23/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

interface BreedApi{
    @GET("general/breed")
    suspend fun getListBreeds(): Response<BaseResponse<List<BreedResponse>>>

    //suspend fun getListBreeds(): Response<GetListBreedsServerResponse>
}