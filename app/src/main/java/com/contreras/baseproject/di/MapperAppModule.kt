package com.contreras.baseproject.di

import com.contreras.baseproject.mapper.BreedMapper
import com.contreras.baseproject.mapper.BreedMapperImpl
import org.koin.dsl.module

/** Created by Renso Contreras on 23/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

val mapperAppModule = module {
    single<BreedMapper> { BreedMapperImpl() }
}