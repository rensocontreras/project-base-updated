package com.contreras.data.network.mapper

import com.contreras.data.network.response.BreedResponse
import com.contreras.domain.model.BreedEntity

/** Created by Renso Contreras on 23/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

interface BreedMapper {
    suspend fun breedDataToDomain(response: BreedResponse): BreedEntity
    suspend fun breedListToDataToDomain(responseList: List<BreedResponse>): List<BreedEntity>

    class BreedMapperImpl : BreedMapper {
        override suspend fun breedDataToDomain(response: BreedResponse): BreedEntity {
            return BreedResponse.toBreedEntity(response)
        }

        override suspend fun breedListToDataToDomain(responseList: List<BreedResponse>): List<BreedEntity> {
            return BreedResponse.toListBreedEntity(responseList)
        }
    }
}