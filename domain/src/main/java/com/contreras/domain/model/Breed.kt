package com.contreras.domain.model

/** Created by Renso Contreras on 06/12/2020.
 * rcontreras@peruapps.com.pe
 * Lima, Peru.
 **/

data class Breed(
    val id: String?,
    val name: String?)