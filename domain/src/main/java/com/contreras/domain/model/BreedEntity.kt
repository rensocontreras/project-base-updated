package com.contreras.domain.model

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

data class BreedEntity (
    val id: String?,
    val name: String?)