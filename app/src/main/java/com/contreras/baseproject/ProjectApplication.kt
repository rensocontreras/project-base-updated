package com.contreras.baseproject

import android.app.Application
import android.content.Context
import com.contreras.baseproject.di.mapperAppModule
import com.contreras.baseproject.di.viewModelModule
import com.contreras.data.di.dataSourceModule
import com.contreras.data.di.mapperDataModule
import com.contreras.data.di.networkModule
import com.contreras.data.di.repositoryModule
import com.contreras.domain.di.useCasesModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

class ProjectApplication: Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: ProjectApplication? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@ProjectApplication)
            modules(arrayListOf(networkModule,
                                repositoryModule,
                                dataSourceModule,
                                mapperDataModule,
                                useCasesModule,
                                mapperAppModule,
                                viewModelModule))
        }
    }
}