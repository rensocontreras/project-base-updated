package com.contreras.data.network.remote.retrofit

import com.contreras.data.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

object ApiClient {
    private const val BASE_URL = BuildConfig.URL
    private const val MAX_TIMEOUT = 60

    fun create(): Retrofit {
        val builder = OkHttpClient.Builder()

        builder.connectTimeout(MAX_TIMEOUT.toLong(), TimeUnit.SECONDS)
        builder.readTimeout(MAX_TIMEOUT.toLong(), TimeUnit.SECONDS)
        builder.writeTimeout(MAX_TIMEOUT.toLong(), TimeUnit.SECONDS)

        builder.addInterceptor(interceptor())

        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .client(builder.build())
            .build()
    }

    private fun interceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor   = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }
}
