package com.contreras.domain.extra

/** Created by Renso Contreras on 22/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

sealed class Result<out T> {
    data class Success<T>(val response: T?) : Result<T>()
    data class Failure(val exception: Exception) : Result<Nothing>()
}

class FailureNetwork(message: String?): Exception(message)
class FailureUnauthorized(message: String?): Exception(message)
class FailureServer(message: String?): Exception(message)
class FailureTimeout(message: String?): Exception(message)
class FailureDataNullError(message: String?): Exception(message)
class FailureDataToDomainMapper(message: String?): Exception(message)
class FailureNetworkConnectionLostSuddenly(message: String?): Exception(message)
class FailureNone(message: String?): Exception(message)
