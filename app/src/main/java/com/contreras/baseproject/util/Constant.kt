package com.contreras.baseproject.util

/** Created by Renso Contreras on 29/11/2020.
 * rensocontreras91@gmail.com
 * Lima, Peru.
 **/

class Constant {

    object Breed{
        const val BUNDLE_LIST_BREED            = "BUNDLE_LIST_BREED"
    }

}